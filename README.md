# 4 Hard Drive Bay with 140-mm Fan Shroud 

A 4 hard drive bay with a 140-mm fan shroud that can be printed using an FDM-based 3D printer. It is created in FreeCAD. The filament material it is designed for is PLA. Could be printed with PETG as the filament, but not tested.

Designed to mount inside of a Fractal Design Define S mark 1 on the bottom. Could be mounted on top, but feared of the plastic breaking apart and hard drives falling down and damage the hard drives. A microATX motherboard is preferred if using this cage with the described computer chassis.

## Author
Designed by: Jason Urbank

## Screen Shots
![](./pictures/view02.png)
![](./pictures/view01.png)

## Software Used
* FreeCAD
* PrusaSlicer

## Do Note
I used Prusa Mini Plus. The temperature that I used is for my 3D printer. My printer is a "demon" 3D printer. It is 10 degrees C out of spec as of firmware version v???. I did not modify it. It came as is. I measured the temperature after I did a "Atomic Pull" and placed a thermocoupler inside the hot nozzle where filament would travel. The temperature equipment that I used is the following.

* NiceTy DT1311
* Minnesota Measurement Instruments K-Type Thermocouple for Digital Thermometers PK-400

Please adjust the printer temperature that suits your printer, filament, and other conditions that may affect the job.

## FDM based 3D Printer Used
Prusa Mini Plus

## FDM Print Job Settings - Hard Drive Cage Only
Quality settings at 0.20-mm layer height
* 1/3 speed override

Estimated Time: 15 Hours and 13 minutes

## FDM Print Job Settings - 140-mm Fan Shroud Only
Quality settings at 0.20-mm layer height
* 1/3 speed override
* No Supports Require

Estimated Time: 8 Hours and 42 minutes

## License
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
